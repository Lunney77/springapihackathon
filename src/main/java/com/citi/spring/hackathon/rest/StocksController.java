package com.citi.spring.hackathon.rest;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.citi.spring.hackathon.entities.Stocks;
import com.citi.spring.hackathon.service.StockService;

@RestController
@RequestMapping("/api/stocks")
public class StocksController {
    @Autowired
    private StockService stockService;

    
    @RequestMapping(method = RequestMethod.GET)
    public Collection<Stocks> getStocks(){
        return stockService.getStocks();
    }

    
    @RequestMapping(method = RequestMethod.POST)
    public void addStock(@RequestBody Stocks c){
    stockService.addStock(c);
    }
    
}