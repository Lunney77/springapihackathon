package com.citi.spring.hackathon.repo;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.citi.spring.hackathon.entities.Stocks;

public interface StockRepo extends MongoRepository<Stocks,ObjectId>{
    
    
    
}