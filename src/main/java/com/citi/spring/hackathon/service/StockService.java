package com.citi.spring.hackathon.service;
import java.util.Collection;

import com.citi.spring.hackathon.entities.Stocks;

import org.springframework.stereotype.Service;


@Service
public interface StockService {

    void addStock(Stocks s);
    Collection<Stocks> getStocks();
    
    
}