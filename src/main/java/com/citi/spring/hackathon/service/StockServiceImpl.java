package com.citi.spring.hackathon.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;

import com.citi.spring.hackathon.entities.Stocks;
import com.citi.spring.hackathon.repo.StockRepo;

public class StockServiceImpl implements StockService {

    @Autowired
    private StockRepo repo;

    @Override
    public void addStock(Stocks s) {
        repo.insert(s);

    }

    @Override
    public Collection<Stocks> getStocks() {
        // TODO Auto-generated method stub
        return repo.findAll();
    }


    
}